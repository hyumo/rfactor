Contains the native code for the rFactor and rFactor 2 plugins.

The rFactor plugin is a Visual Studio Express C++ 2010 project.
The rFactor 2 plugin is a Visual Studio Express 2013 project.
