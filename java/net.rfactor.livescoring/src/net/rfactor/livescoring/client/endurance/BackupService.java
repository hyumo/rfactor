package net.rfactor.livescoring.client.endurance;

import java.util.SortedSet;

/**
 * Backup service for saving various kinds of standings during the race.
 */
public interface BackupService {
	/**
	 * Backup the current race standings as if the race had ended this very second.
	 */
	public void backupStandings(SortedSet<Vehicle> standings, double eventTime, boolean useInternalScoring);
	/**
	 * Backup the current lap standings in the order the cars would have to drive behind the pace car
	 * in case of a full-course yellow.
	 */
	public void backupLapStandings(SortedSet<Vehicle> standings, double eventTime, boolean useInternalScoring);
	/**
	 * Write the final standings to disk.
	 */
	public void writeFinalStandings(SortedSet<Vehicle> standings);
}
