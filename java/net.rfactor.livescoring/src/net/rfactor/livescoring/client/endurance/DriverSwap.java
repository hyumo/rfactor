package net.rfactor.livescoring.client.endurance;

import net.rfactor.livescoring.VehicleData;

public class DriverSwap {
    private final double m_et;
    private final VehicleData m_oldDriverVehicleData;
    private final VehicleData m_newDriverVehicleData;

    public DriverSwap(double et, VehicleData data, VehicleData data2) {
        m_et = et;
        m_oldDriverVehicleData = data;
        m_newDriverVehicleData = data2;
    }
    
    public String getFromDriver() {
    	return m_oldDriverVehicleData.getDriverName();
    }
    
    public String getToDriver() {
    	return m_newDriverVehicleData.getDriverName();
    }
    
    public double getTime() {
		return m_et;
	}
}
