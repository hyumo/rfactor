package net.rfactor.livescoring.client.endurance.impl;

import java.util.Comparator;

import net.rfactor.livescoring.client.endurance.Vehicle;

/**
 * Compares the rFactor positions of the cars.
 */
public class RfactorPositionComparator implements Comparator<Vehicle> {
    public int compare(Vehicle o1, Vehicle o2) {
        return o2.getData().getPlace() < o1.getData().getPlace() ? 1 : -1;
    }
}