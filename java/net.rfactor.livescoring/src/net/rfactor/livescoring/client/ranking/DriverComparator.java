package net.rfactor.livescoring.client.ranking;

import java.util.Comparator;

public class DriverComparator implements Comparator<Driver> {
	@Override
	public int compare(Driver d1, Driver d2) {
		Car c1 = d1.getFastestCar();
		Car c2 = d2.getFastestCar();
		double l1 = (c1 == null) ? Double.MAX_VALUE : (c1.getFastestLapTime() < 0.1f ? Double.MAX_VALUE : c1.getFastestLapTime());
		double l2 = (c2 == null) ? Double.MAX_VALUE : (c2.getFastestLapTime() < 0.1f ? Double.MAX_VALUE : c2.getFastestLapTime());
		if (l1 == Double.MAX_VALUE && l2 == Double.MAX_VALUE) {
			return d1.getName().compareTo(d2.getName());
		}
		return (int) Math.signum(l1 - l2);
	}
}
