package net.rfactor.racecontrol.ui;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class VaadinResourceHandler {
    private volatile HttpService m_http;
    private HttpContext m_context;
    
    public void start() {
        m_context = m_http.createDefaultHttpContext();
        try {
            m_http.registerResources("/VAADIN", "/VAADIN", new HttpContext() {
                public String getMimeType(String name) {
                    return m_context.getMimeType(name);
                }

                /**
                 * We use a slightly modified version of the 'reindeer' theme. To avoid having
                 * to copy all resources in the Vaadin jar, we only override the files we changed
                 * and do replace the theme name 'rc' with 'reindeer' before we go looking for the
                 * original files.
                 * 
                 * When updating to a new Vaadin version, usually you need to copy the styles.css
                 * file from the original archive again and append the changes to the end, as this
                 * file tends to change considerably between versions.
                 */
                public URL getResource(String name) {
                    URL resource = null;
                    String prefix = "/VAADIN/";
                    if (!name.startsWith("/")) {
                        name = "/" + name;
                    }
                    if (name.startsWith(prefix)) {
                    	String originalName = name.replace("/rc/", "/reindeer/");
                        resource = getClass().getResource(originalName);
                        if (resource == null) {
                            // try to find the resource in the Vaadin bundle instead
                            resource = com.vaadin.Application.class.getResource(originalName);
                        }
                    }
                    return resource;
                }

                public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
                    return m_context.handleSecurity(request, response);
                }});
        }
        catch (NamespaceException e) {
            e.printStackTrace();
        }
    }
}
