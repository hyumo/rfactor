package net.rfactor.serverlog.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rfactor.serverlog.ServerLog;
import net.rfactor.util.HtmlUtil;
import net.rfactor.util.TimeUtil;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class ServerLogImpl extends HttpServlet implements ServerLog {
	private static final String ENDPOINT = "/log";
	private static final int IN_MEMORY_LINE_LIMIT = 5000; // maximum number of lines to keep in memory
	private final List<LogEntry> m_log = new ArrayList<LogEntry>();
    private File m_logFileName = new File("serverlog.txt"); // TODO make the log file name configurable
    private volatile EventAdmin m_eventAdmin;

	@Override
	public void log(double eventTime, String type, Map<String, Object> attributes) {
	    synchronized (m_log) {
	        if (m_log.size() > IN_MEMORY_LINE_LIMIT) {
	            m_log.remove(0);
	        }
	        m_log.add(new LogEntry(eventTime, type, attributes));
	        Map<String, Object> eventProps = new HashMap<String, Object>(attributes);
	        eventProps.put("eventTime", eventTime);
	        m_eventAdmin.postEvent(new Event("serverlog/" + type, eventProps));
	    }
	    StringBuilder msg = new StringBuilder();
	    msg.append(type + "; ");
	    boolean first = true;
	    for (Entry<String, Object> e : attributes.entrySet()) {
	        if (first) {
	            first = false;
	        }
	        else {
	            msg.append(", ");
	        }
	        msg.append(e.getKey() + "=\"" + encodeValue(e.getValue()) + "\"");
	    }
	    writeToLogFile(eventTime, msg.toString());
	}
	
    private String encodeValue(Object value) {
    	if (value == null) {
    		return "<null>";
    	}
        String result = value.toString();
        // replace backslashes, double quotes, new lines, carriage returns and tabs
        result = result.replace("\\", "\\\\");
        result = result.replace("\"", "\\q");
        result = result.replace("\n", "\\n");
        result = result.replace("\r", "\\r");
        result = result.replace("\t", "\\t");
        return result;
    }
	
	@Override
	public void log(double eventTime, String type, Object... attributes) {
		if (attributes.length % 2 != 0) {
			throw new IllegalArgumentException("Attributes should be a multiple of 2: key and values.");
		}
		Map<String, Object> attrs = new HashMap<String, Object>();
		for (int i = 0; i < attributes.length; i += 2) {
			attrs.put((String) attributes[i], attributes[i + 1]);
		}
		log(eventTime, type, attrs);
	}

    private void writeToLogFile(double eventTime, String message) {
        // TODO opening and flushing after each message might be too costly, so we should
        // consider only flushing to disk periodically (or when the program or session ends)
        synchronized (m_logFileName) {
            FileWriter fw = null;
            try {
                // append to any existing file
                fw = new FileWriter(m_logFileName, true);
                fw.write(TimeUtil.toLapTime(eventTime) + "; " + message + "\n");
            }
            catch (IOException e) {
                // for some reason we could not write to the file
                // and for now we just print that exception to the console
                e.printStackTrace();
            }
            finally {
                if (fw != null) {
                    try {
                        // and close the file, so we are sure it is flushed to disk
                        fw.close();
                    }
                    catch (IOException e) {
                    }
                }
            }
        }
    }
	
	@Override
	public void reset() {
        synchronized (m_log) {
        	// find a still unused file by appending a number
        	File rotatedLog = null;
        	for (int i = 1; i < Integer.MAX_VALUE; i++) {
				rotatedLog = new File("serverlog-" + i + ".txt");
        		if (!rotatedLog.exists()) {
        			break;
        		}
        	}
        	// rotate the current log file
        	m_logFileName.renameTo(rotatedLog);
        	// clear it
            m_log.clear();
        }
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter writer = resp.getWriter();
		resp.setContentType("text/html");
		writer.println(HtmlUtil.PageHeader("Server Log", 5));
		/*
		writer.write("<html>");
		writer.write("<head><meta http-equiv=\"refresh\" content=\"5\"/><title>Hot Laps</title><style>");
		writer.write("body { font-family: sans-serif; } " +
				"table { border: solid 2px #000; } " +
				"td { margin: 0px; background-color: #eee; text-align: center; font-size: 10pt; padding: 3px; } " +
		"th { color: #fff; text-align: center; background-color: #e40; font-size: 11pt; padding: 5px; } "); 
		writer.write("</style></head>");
		writer.write("<body>");
		*/
		writer.println("<h1>Server Log</h1>");
		writer.println("<table cellspacing=1>");
		writer.println("<tr><th>Event Time</th><th>Type</th><th>Attributes</th></tr>");
	    synchronized (m_log) {
	        // iterate over the log backwards so we can show the newest item at the top of the list
	        for (int i = m_log.size() - 1; i >= 0; i--) {
	            LogEntry entry = m_log.get(i);
	            StringBuilder attrs = new StringBuilder();
	            for (String key : entry.getAttributeKeys()) {
	            	if (attrs.length() > 0) {
	            		attrs.append(",");
	            	}
	            	attrs.append(key);
	            	attrs.append('=');
	            	attrs.append(entry.getAttributeValue(key));
	            }
    			writer.println("<tr>" +
    					"<td>" + TimeUtil.toLapTime(entry.getEventTime()) + "</td>" +
    					"<td>" + entry.getType() + "</td>" +
    					"<td>" + attrs + "</td>" +
    					"</tr>");
    		}
	    }
		writer.println("</table>");
		//writer.write("</body></html>");
		writer.println(HtmlUtil.PageFooter());
	}
	
    public void addHttpService(HttpService http) {
        try {
            http.registerServlet(ENDPOINT, this, null, null);
        }
        catch (ServletException e) {
            e.printStackTrace();
        }
        catch (NamespaceException e) {
            e.printStackTrace();
        }
    }

    public void removeHttpService(HttpService http) {
        http.unregister(ENDPOINT);
    }
}
