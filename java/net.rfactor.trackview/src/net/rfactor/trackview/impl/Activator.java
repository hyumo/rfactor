package net.rfactor.trackview.impl;


import java.util.Properties;

import net.rfactor.livescoring.LiveScoringConstants;
import net.rfactor.trackview.TrackViewerConstants;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, TrackViewerConstants.TRACKVIEWER_PID);
        props.put(EventConstants.EVENT_TOPIC, LiveScoringConstants.TOPIC);
//        props.put(EventConstants.EVENT_FILTER, "(" + LiveScoringConstants.SCORINGSERVER + "=server-6789)");
        manager.add(createComponent()
            .setInterface(new String[] { EventHandler.class.getName(), ManagedService.class.getName() }, props)
            .setImplementation(TrackViewer.class)
            .add(createServiceDependency()
                .setService(HttpService.class)
                .setRequired(false)
                .setAutoConfig(false)
                .setCallbacks("addHttpService", "removeHttpService")
                )
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
