package net.rfactor.trackview.impl;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.SequenceInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TrackImpl {
	public float m_xmin, m_xmax, m_ymin, m_ymax;
	public float[] m_x, m_y, m_w1, m_w2, m_p1, m_p2;
	public int[] m_next, m_prev;
    private String m_name;
    private String m_folder;
    private int m_pitOffset;
    private int m_pitEndOffset;
    // TODO make these accessible
    private String m_venueName;
    private String m_eventName;
    private String m_trackName;
    private String m_location;
    private enum State { NOTRACK, LOADING, ACTIVE };
	private State m_state;
    private float m_scale;
    private int m_offset;
    private float m_maxWidth = 700.0f;
    private float m_maxHeight = 700.0f;
	
	public TrackImpl() {
	    m_state = State.NOTRACK;
	}
	
	public float getMaxWidth(){
		return m_maxWidth;
	}
	
	public void setMaxWidth(float maxWidth){
	    if (maxWidth < 150.0f) maxWidth = 150.0f;
	    if (maxWidth > 2000.0f && maxWidth != 9999.9f) maxWidth = 2000.0f;
		m_maxWidth = maxWidth;
	}
	
	public float getMaxHeight(){
		return m_maxHeight;
	}
	
	public void setMaxHeight(float maxHeight){
	    if (maxHeight < 150.0f) maxHeight = 150.0f;
	    if (maxHeight > 2000.0f && maxHeight != 9999.9f) maxHeight = 2000.0f;
		m_maxHeight = maxHeight;
	}
	
	public float getXmax() {
        return m_xmax;
    }
	public float getXmin() {
        return m_xmin;
    }
	public float getYmax() {
        return m_ymax;
    }
	public float getYmin() {
        return m_ymin;
    }

	public void setTrackName(String folder, String name, float maxWidth, float maxHeight)  throws IOException {
		m_folder = folder;
	    m_name = name;
	    setMaxWidth(maxWidth);
	    setMaxHeight(maxHeight);
	    reloadTrack();
	}
	
	public boolean reloadTrack() throws IOException {
		if (m_name == "") return false;
		if (m_folder == "") return false;
        Map<String, File> aiws = new HashMap<String, File>();
	    scan(aiws, new File(m_folder));
	    File f = aiws.get(m_name);
	    if (f != null) {
            String fileName = f.getAbsolutePath();
            String gdbFileName = fileName.substring(0, fileName.length() - 3) + "gdb";
            File f2 = new File(gdbFileName);
            if (f.isFile() && f2.isFile()) {
                FileInputStream fis = new FileInputStream(f);
                FileInputStream fis2 = new FileInputStream(f2);
                SequenceInputStream sis = new SequenceInputStream(fis, fis2);
                load(sis);
                return true;
            }
	    }
	    return false;
	}
	
	public String getTrackName() {
	    return m_name;
	}
	
	public void setFile(File file) throws IOException {
		load(new FileInputStream(file));
	}
	
	public void load(InputStream is) throws IOException {
	    synchronized (this) {
	        m_state = State.LOADING;
	        m_xmin = 0;
	        m_xmax = 0;
	        m_ymin = 0;
	        m_ymax = 0;
        }
	    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = br.readLine();
		int i = 0;
		while (line != null) {
			if (line.startsWith("number_waypoints")) {
				int nr = Integer.parseInt(line.substring(line.indexOf('=') + 1));
//				System.out.println("Waypoints : " + nr);
				m_x = new float[nr];
				m_y = new float[nr];
				m_w1 = new float[nr];
				m_w2 = new float[nr];
                m_p1 = new float[nr];
                m_p2 = new float[nr];
				m_next = new int[nr];
				m_prev = new int[nr];
			} 
			else if (line.startsWith("wp_pos")) {
				int i1 = line.indexOf('(');
				int i2 = line.indexOf(',');
				int i3 = line.lastIndexOf(',');
				int i4 = line.indexOf(')');
				m_x[i] = Float.parseFloat(line.substring(i1 + 1, i2));
				m_y[i] = Float.parseFloat(line.substring(i3 + 1, i4));
				//System.out.println("" + x[i] + "," + y[i]);

				m_xmin = Math.min(m_xmin, m_x[i]);
				m_xmax = Math.max(m_xmax, m_x[i]);
				m_ymin = Math.min(m_ymin, m_y[i]);
				m_ymax = Math.max(m_ymax, m_y[i]);
			} 
			else if (line.startsWith("wp_width")) {
				int i1 = line.indexOf('(');
				int i2 = line.indexOf(',');
				int i3 = line.indexOf(',', i2 + 1);
				int i4 = line.lastIndexOf(',');
				int i5 = line.indexOf(')');
				m_w1[i] = Float.parseFloat(line.substring(i1 + 1, i2));
				m_w2[i] = Float.parseFloat(line.substring(i2 + 1, i3));
//				System.out.println("" + w1[i] + "," + w2[i]);
			} 
			else if (line.startsWith("WP_PTRS")) {
				int i1 = line.indexOf('(');
				int i2 = line.indexOf(',');
				int i3 = line.indexOf(',', i2 + 1);
                int i4 = line.indexOf(',', i3 + 1);
                int i5 = line.indexOf(')');
				m_prev[i] = Integer.parseInt(line.substring(i1 + 1, i2));
				m_next[i] = Integer.parseInt(line.substring(i2 + 1, i3));
				m_p1[i] = Integer.parseInt(line.substring(i3 + 1, i4));
                m_p2[i] = Integer.parseInt(line.substring(i4 + 1, i5));
				//System.out.println(i + ":" + prev[i] + "," + next[i]);
				i++;
			}
			else {
			    line = line.trim();
			    int equalsPos = line.indexOf('=');
			    if (equalsPos > 0) {
    			    if (line.startsWith("VenueName")) {
    			        m_venueName = line.substring(equalsPos + 1).trim();
    			    }
    			    else if (line.startsWith("EventName")) {
                        m_eventName = line.substring(equalsPos + 1).trim();
    			    }
                    else if (line.startsWith("TrackName")) {
                        m_trackName = line.substring(equalsPos + 1).trim();
                    }
                    else if (line.startsWith("Location")) {
                        m_location = line.substring(equalsPos + 1).trim();
                    }
			    }
			}
			//System.out.println(line);
			line = br.readLine();
		}
		
		// normalize track coordinates
		// for now :)
	    float xs = m_xmax - m_xmin;
	    float ys = m_ymax - m_ymin;
	    
	    float sw = (m_maxWidth == 9999.9f ? 99.9f : (m_maxWidth / xs));
	    float sh = (m_maxHeight == 9999.9f ? 99.9f : (m_maxHeight / ys));
	    
	    m_scale = Math.min(sw, sh);
	    
		for (int j = 0; j < m_x.length; j++) {
		    m_x[j] -= m_xmin;
		    m_y[j] -= m_ymin;
		    /*
		    float xs = m_xmax - m_xmin;
		    float ys = m_ymax - m_ymin;
		    
		    m_scale = m_maxWidth / Math.max(xs, ys);
		    */
		    
		    m_offset = 10;
            m_x[j] = m_offset + m_x[j] * m_scale;
            m_y[j] = m_offset + m_y[j] * m_scale;
		}
		
		synchronized (this) {
		    m_state = State.ACTIVE;
		    m_pitOffset = -1;
        }
		// initial zoom
		// TODO we might need this (!)
//        float dx = m_xmax - m_xmin;
//        float dy = m_ymax - m_ymin;
//        float sx = getWidth() / dx;
//        float sy = getHeight() / dy;
//        float s = Math.min(sx, sy);
//        m_scale.setWorldCenter(new Point2D.Double((m_xmax + m_xmin) / 2.0, (m_ymax + m_ymin) / 2.0));
//        m_scale.setWorldScale((double) s);
//        setZoomTrack(true);
	}
	
	private int getPitOffset() {
	    int i1 = 0;
        if (m_pitOffset == -1) {
            for (int i = 0; i < m_p1.length; i++) {
                if ((m_p1[i] == -1) && (m_p2[i] == 1)) {
                    while (m_p2[i] >= 1) {
                        i1 = i;
                        i = m_prev[i];
                    }
                    break;
                }
            }
            m_pitOffset = i1;
            i1 = m_next[i1];
            while (m_p2[i1] >= 1) {
                i1 = m_next[i1];
            }
            m_pitEndOffset = i1;
        }
        return m_pitOffset;
	}
	
	private int getPitEndOffset() {
		getPitOffset();
		return m_pitEndOffset;
	}
	
	public Iterator<Point2D> getTrackIterator() {
	    return new SetpointIterator();
	}
	
	public Iterator<Point2D> getPitlaneIterator() {
	    return new SetpointIterator(getPitOffset(), getPitEndOffset());
	}
	
	class SetpointIterator implements Iterator<Point2D> {
	    private int i1, i2;
	    
	    public SetpointIterator() {
	        i1 = 0;
	        i2 = -1;
	    }
	    
	    public SetpointIterator(int start, int end) {
	        i1 = start;
	        i2 = end;
	    }
	    
        public boolean hasNext() {
        	if (i2 == -1) {
        		return i1 >= 0;
        	}
        	else {
        		return (i2 != i1);
        	}
        }

        public Point2D next() {
            Point2D p = new Point2D.Double(m_x[i1], m_y[i1]);
            i1 = m_next[i1];
            if (i1 == 0) {
                // back at the start
                i1 = -1;
            }
            return p;
        }

        public void remove() {
            throw new UnsupportedOperationException("Can't remove setpoints from a track.");
        }
	}
	
	public synchronized boolean isLoaded() {
	    return m_state == State.ACTIVE;
	}
    public synchronized boolean isLoading() {
        return m_state == State.LOADING;
    }
    public synchronized boolean isIdle() {
        return m_state == State.NOTRACK;
    }
	
		
	private static void scan(Map<String, File> entries, File dir) {
        File[] files = dir.listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                scan(entries, f);
            }
            else {
                // might be a file we're interested in...
                if (f.getName().toLowerCase().endsWith(".gdb")) {
                    try {
                        // try to read the gdb file, we're looking for the track name
                        BufferedReader br = new BufferedReader(new FileReader(f));
                        String line = br.readLine();
                        while (line != null) {
                            String trimmedLine = line.trim();
                            if (trimmedLine.startsWith("TrackName")) {
                                int i = trimmedLine.indexOf('=');
                                String trackName = trimmedLine.substring(i + 1).trim();
                                String gdb = f.getAbsolutePath();
                                File aiw = new File(gdb.substring(0, gdb.length() - 4) + ".aiw");
                                entries.put(trackName, aiw);
                            }
                            line = br.readLine();
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public String getTrackLocation() {
        return m_location;
    }

    public String getScaleData() {
        return "{ \"xmin\": " + m_xmin + ", \"ymin\": " + m_ymin + ", \"scale\": " + m_scale +  ", \"offset\": " + m_offset + " }";
    }
}
