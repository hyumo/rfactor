package net.rfactor.trackview.impl;


import java.awt.geom.Point2D;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rfactor.livescoring.LiveScoringConstants;
import net.rfactor.livescoring.ScoringData;
import net.rfactor.livescoring.VehicleData;
import net.rfactor.trackview.TrackViewerConstants;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class TrackViewer extends HttpServlet implements EventHandler, ManagedService {
    private static final long serialVersionUID = 1L;
    private static final String ENDPOINT = "/track";
    private static final String DEFAULT_RFACTORFOLDER = "/Racing/rFactor-SR.org-test";
    private static boolean m_showDriverNames = false; // True = Drivernames, false = Vehiclenames
    private double m_lastEt = -1f;
    private String m_names;
    private String m_values;
	private long m_namesVersion = 1;
	private long m_valuesVersion = 1;
	private final boolean m_includePosition = true; // write current position in front of the name
	private final boolean m_parseDriver = true;	 // parse driver name to firstname, lastname (+ number)
	private final int m_lengthFirstname = 1;		 // if m_parseDriver=true, shorten the firstname to m_lengthFirstname letteres. negative value means no shortening
	private final int m_lengthLastname = 2;			 // if m_parseDriver=true, shorten the lastname to m_lengthLastname letteres. negative value means no shortening
	private DecimalFormat decimals = new DecimalFormat("#.##");
	private TrackImpl m_currentTrack = new TrackImpl();
	private String m_rFactorFolder = DEFAULT_RFACTORFOLDER;
	private float m_maxWidth = 700.0f;
	private float m_maxHeight = (float)(m_maxWidth * 2.0 / 3.0);
	private List<String> m_dataLines = new LinkedList<String>();
	
    public void handleEvent(Event event) {
        Locale.setDefault(Locale.US);
        decimals = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        decimals.applyPattern("#.##");
        ScoringData scoringData = (ScoringData) event.getProperty(LiveScoringConstants.SCORINGDATA);
        if (scoringData != null) {
            double et = scoringData.getEventTime();
            if (m_lastEt < 0) {
                m_lastEt = et;
            }
            if (m_lastEt > et) {
//                resetSession();
            }
            
            String trackName = scoringData.getTrackName();
            if (trackName != null && !trackName.equals(m_currentTrack.getTrackName())) {
            	// load new track
            	try {
					m_currentTrack.setTrackName(m_rFactorFolder, trackName, m_maxWidth, m_maxHeight);
				}
            	catch (IOException e) {
					e.printStackTrace();
				}
            }
            
            m_lastEt = et;
            VehicleData[] vehicleData = scoringData.getVehicleScoringData();
            StringBuilder names = new StringBuilder("et");
            StringBuilder values = new StringBuilder("" + decimals.format(m_lastEt));
            try {
	            for (VehicleData data : vehicleData) {
	            	
	            	/*
	            	String name;
	            
	            	if (m_showDriverNames){
	            		name = data.getDriverName();
	            	}else{
	            		name = data.getVehicleName();
	            	}
	            	*/
	            	String name = buildName(data);
	                names.append(";");
	                names.append(name);
	                values.append(";");
	                values.append("" + decimals.format(data.getXPosition()) + "," + decimals.format(data.getZPosition()));
	            }
	            if (!names.toString().equals(m_names)) {
	            	// the names have changed ;)
	            	m_names = names.toString();
	            	m_namesVersion++;
	            	
	            	addDataLine(names.toString());
	            }
			}
        	catch (Exception e) {
				e.printStackTrace();
			}
            m_values = values.toString();
            m_valuesVersion++;
            addDataLine(values.toString());
        }
    }
    
    private void addDataLine(String line) {
    	synchronized (m_dataLines) {
	    	m_dataLines.add(line);
	    	while (m_dataLines.size() > 20) {
	    		m_dataLines.remove(0);
	    	}
    	}
    }

    private String getDataLines(String et) {
    	double latest = (et != null) ? Double.valueOf(et) : 0;
    	StringBuffer result = new StringBuffer();
    	synchronized (m_dataLines) {
    		boolean startHere = (et == null) ? true : false;
    		for (String line : m_dataLines) {
    			if (startHere) {
	    			result.append(line);
	    			result.append('\n');
    			}
    			else if (et != null) {
    				int p = line.indexOf(";");
    				if (p > 0) {
    					String lineEt = line.substring(0, p);
    					try {
	    					if (Double.valueOf(lineEt) > latest) {
	    						startHere = true;
	    					}
    					}
    					catch (NumberFormatException nfe) {
    						// if et was not a number, for now simply ignore it
    					}
    				}
    			}
    		}
    	}
    	return result.toString();
    }
    
    /**
     * build the text for the vehicle label
     * @param v VehicleData
     * @return String
     */
    private String buildName(VehicleData v){
    	String name = "";
    	if (m_showDriverNames){
    		name = v.getDriverName();
    		if (m_parseDriver){
    			String[]items = name.split(" ");
    			String num="", fname="", lname="";
    			if (items.length == 1){
    				// seems to be only the lastname
    				lname = items[0].trim();
    			}
    			else if (items.length == 2){
    				// "number lastname" or "firstname lastname"
    				if (isNum(items[0])){
    					num = items[0].trim();
    				}else{
    					fname = items[0].trim();
    				}
    				lname = items[1].trim();
    			}
    			else if (items.length > 2){
    				// "number firstname lastname" or "firstname anothername andAnotherName lastname"
    				if (isNum(items[0])){
    					num = items[0].trim();
    					fname = items[1].trim();
    				}else{
    					fname = items[0].trim();
    				}
    				lname = items[items.length-1].trim();
    			}
    			try{
    			if (!fname.equals("")) {
	    			if (m_lengthFirstname == 0) fname = "";
	    			else if (m_lengthFirstname > 0 && fname.length() > m_lengthFirstname) fname = fname.substring(0, m_lengthFirstname).toUpperCase();
	    			else fname += " ";
    			}
    			if (!lname.equals("")) {
	    			if (m_lengthLastname == 0) lname = "";
	    			else if (m_lengthLastname > 0 && lname.length() > m_lengthLastname) lname = lname.substring(0, m_lengthLastname).toUpperCase();
    			}
    			}
            	catch (Exception e) {
    				e.printStackTrace();
    			}

    			name = (num != "" ? num + " " : "") + fname + lname;
    		}
    	}else{
    		name = v.getVehicleName();
    	}
    	if (m_includePosition){
    		name = "(" + v.getPlace() + ") " + name;
    	}
    	return name;
    }
    
    public static boolean isNum(String s) {
    	try {
    		Integer.parseInt(s);
    	}
    	catch (Exception e) {
    		return false;
    	}
    	return true;
    }
    
    public void addHttpService(HttpService http) {
        try {
            http.registerServlet(ENDPOINT, this, null, null);
        }
        catch (ServletException e) {
            e.printStackTrace();
        }
        catch (NamespaceException e) {
            e.printStackTrace();
        }
    }

    public void removeHttpService(HttpService http) {
        http.unregister(ENDPOINT);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String subPath = req.getPathInfo();
		try{
			m_maxWidth = 0f;
			m_maxWidth = Float.parseFloat(req.getParameterValues("w")[0]);
		}catch(Exception e){}
		try{
			m_maxHeight = 0f;
			m_maxHeight = Float.parseFloat(req.getParameterValues("h")[0]);
		}catch(Exception e){}
		if (m_maxWidth <= 0.0f && m_maxHeight <= 0.0f) m_maxWidth = 700.0f;
		if (m_maxHeight <= 0.0f) m_maxHeight = 9999.9f;
		else if (m_maxWidth <= 0.0f) m_maxWidth = 9999.9f;
		try{
			m_showDriverNames = true;
			String show = req.getParameterValues("s")[0];
			if(show.equals("vehicle")){
				m_showDriverNames = false;
			}
			m_namesVersion++;
		}catch(Exception e){}
		
		
//		// we really want the path to end with a '/' so if it does not, we redirect to a version that does
//		if (subPath.length() == 0) {
//			resp.sendRedirect(servletPath + "/");
//			return;
//		}
//		
		// depending on the path, this might be a request for a static resource, so thy that first
		if (subPath.startsWith("/css/") || subPath.startsWith("/js/") || subPath.startsWith("/images/") || subPath.endsWith(".html")) {
			if ((subPath.toLowerCase().endsWith("track.html") || (subPath.toLowerCase().endsWith("track2.html"))) && m_currentTrack != null) {
				if ((m_maxWidth > 0.0f && m_maxWidth != m_currentTrack.getMaxWidth()) || (m_maxHeight > 0.0f && m_maxHeight != m_currentTrack.getMaxHeight())){
					m_currentTrack.setMaxWidth(m_maxWidth);
					m_currentTrack.setMaxHeight(m_maxHeight);
					m_currentTrack.reloadTrack();
				}
			}
			if (subPath.toLowerCase().endsWith(".css")) {
				resp.setContentType("text/css");
			}
			if (subPath.toLowerCase().endsWith(".js")) {
				resp.setContentType("text/javascript");
			}
			if (subPath.toLowerCase().endsWith(".html")) {
				resp.setContentType("text/html; charset=utf-8");
			}
			if (subPath.toLowerCase().endsWith(".gif")) {
				resp.setContentType("image/gif");
			}
			BufferedInputStream bis = null;
			try {
				bis = new BufferedInputStream(this.getClass().getResourceAsStream(subPath));
				ServletOutputStream output = resp.getOutputStream();
				byte[] buffer = new byte[4096];
				int result = bis.read(buffer, 0, buffer.length);
				while (result != -1) {
					output.write(buffer, 0, result);
					result = bis.read(buffer, 0, buffer.length);
				}
				return;
			}
			finally {
				if (bis != null) {
					bis.close();
				}
			}
		}

    	if (subPath.equals("/data")) {
	        resp.setContentType("text/plain");
	        PrintWriter w = resp.getWriter();
	        long namesVersion = 0, valuesVersion = 0;
	        //boolean showDriverNames = false;
	        while (true) {
	        	/*
	        	if (showDriverNames != m_showDriverNames){
	        		showDriverNames = m_showDriverNames;
	        		m_namesVersion++;
	        	}
	        	*/
	        	if (namesVersion != m_namesVersion) {
	        		namesVersion = m_namesVersion;
        			w.println(m_names);
	        	}
	        	if (valuesVersion != m_valuesVersion) {
	        		valuesVersion = m_valuesVersion;
	        		w.println(m_values);
	        	}
	        	w.flush();
	        	try { Thread.sleep(500); } catch (InterruptedException e) {}
	        }
    	}
    	else if (subPath.equals("/line")) {
    		if (m_currentTrack != null) {
    	        resp.setContentType("text/plain");
    	        writeLine(resp.getWriter(), m_currentTrack.getTrackIterator());
    		}
    	}
    	else if (subPath.equals("/pitlane")) {
    		if (m_currentTrack != null) {
    	        resp.setContentType("text/plain");
    	        writeLine(resp.getWriter(), m_currentTrack.getPitlaneIterator(), false);
    		}
    	}
    	else if (subPath.equals("/scale")) {
    	    if (m_currentTrack != null) {
    	        resp.setContentType("application/json");
    	        resp.getWriter().println(m_currentTrack.getScaleData());
    	    }
    	}
    	else if (subPath.equals("/data2")) {
    		if (m_currentTrack != null) {
    			String et = req.getParameter("et");
    			String h = req.getParameter("h");
    			resp.setContentType("text/plain");
    			PrintWriter w = resp.getWriter();
    			if (h != null && m_names != null) {
    				w.write(m_names);
    				w.write('\n');
    			}
    			w.write(getDataLines(et));
    		}
    	}
    }
	private void writeLine(PrintWriter w, Iterator<Point2D> iterator) {
		writeLine(w, iterator, true);
	}

	private void writeLine(PrintWriter w, Iterator<Point2D> iterator, boolean close) {
        Locale.setDefault(Locale.US);

		boolean first = true;
		while (iterator.hasNext()) {
			Point2D p = iterator.next();
			if (first) {
				w.print("M ");
				first = false;
			}
			else {
				w.print("L ");
			}
			w.printf("%.2f %.2f ", p.getX(), 1020 - p.getY());
//			w.print(decimals.format(p.getX()) + " " + decimals.format(p.getY()) + " ");
		}
		if (close) {
			w.print("Z");
		}
	}

    @Override
    public void updated(Dictionary properties) throws ConfigurationException {
        if (properties == null) {
            m_rFactorFolder = DEFAULT_RFACTORFOLDER;
        }
        else {
            String folder = (String) properties.get(TrackViewerConstants.RFACTORFOLDER_KEY);
            if (folder != null) {
                m_rFactorFolder = folder;
//                System.out.println("rFactor Folder: " + m_rFactorFolder);
            }
            else {
                throw new ConfigurationException(TrackViewerConstants.RFACTORFOLDER_KEY, "must be specified");
            }
        }
    }
}
